<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19/12/2017
 * Time: 11:32
 */

function arvuSumma($suvalineNumber){
    $summa = 0;
    while($suvalineNumber){
        $arv = $suvalineNumber % 10;
        $summa = $summa + $arv;
        $suvalineNumber = $suvalineNumber / 10;
    }
    return $summa;
}

    $suvalineNumber = rand(100, 999);
    echo $suvalineNumber.' summa on '.arvuSumma($suvalineNumber);

    ?><br><br><?php

function otsiNumber($suvalineNumber, $number){
    $korrad = 0;
    $arvutuseNumber = $suvalineNumber;
    while($arvutuseNumber){
        $arv = $arvutuseNumber % 10;
        if($arv == $number){
            $korrad++;
        }
        $arvutuseNumber = $arvutuseNumber / 10;
    }
    echo 'Number '.$number.' esineb arvus '.$suvalineNumber.' - '.$korrad.' korda';
}

$nr1 = rand(100000000000, 999999999999);
$nr2 = rand(1, 9);
otsiNumber($nr1, $nr2);